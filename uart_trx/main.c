/*
 * uart_trx.c
 *
 * Created: 2017. 07. 22. 10:38:30
 * Author : DANI
 */ 

/*
1 start bit:		0
8 adat bit:		x
1 parit�s bit:	p
1 stop bit:		1
K�t r�szletben kell adni:
0xxxxxxx xp111111

Baud:		38400 baud
Clock:		1MHz
Division:	26

Minden m�s esetben az adot pin magas kell legyen
*/

/*
Ha p�ros parit�s kell, akkor az adat biteket
kiz�r� vagykapuzni kell, �s az utols� kapuz�s
eredm�nye lesz a parit�s bit.
Az adat bitet a parity mask bittel �s kapuzni kell,
ha az eredm�ny 1, akkor egy parit�s byte-ot neg�lni kell

patity mask byte:	0b00000001

*/

#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 1000000  // 1 MHz
#include <util/delay.h>



static volatile uint8_t status;


ISR(USI_OVF_vect){
	//PORTB |= (1<<PB3);
	if(status == 0) {
		USIDR = 0b00010001;
		USISR |= 0b11111000;
		status += 1;
	}
	else if(status == 1) {
		PORTB ^= 0b00001000;
		USIDR = 0b01111111;
		USISR |= 0b11111110;
		status += 1;
	}
	else {
		TCCR0B = 0b00000000;
		//PORTB ^= (1<<PB1);
	}
	
}

int main(void)
{
	_delay_ms(5000);
	status = 0;
	DDRB |= (1<<PB1)|(1<<PB3);
	PORTB ^= (1<<PB1);
	TCNT0 = 0;
	OCR0A = 51;
	TIMSK = 0b00000000;
	TCCR0A = 0b00000010;
	TCCR0B = 0b00000001;
	sei();
	USICR = 0b01010100;
	USIDR = 0b11111111;
	USISR |= 0xFF;
    while (1) 
    {
    }
}

